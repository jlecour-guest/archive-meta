#!/usr/bin/env python3

import argparse
import re
from datetime import datetime
from pathlib import Path
from time import mktime
from xml.etree import ElementTree

import dateutil.parser
import feedparser
import requests

from utils.objects import Conference, Event, Meta, VideoFormat
from utils.pentabarf import parse_timedelta
from utils.yaml import yaml_dump


def scrape(url, args):
    feed = feedparser.parse(url)

    penta_events = None
    if args.penta:
        r = requests.get(args.penta, verify=args.verify)
        penta_tree = ElementTree.fromstring(r.content)
        penta_events = penta_events_by_talk_id(penta_tree)

    conference = scrape_conference(feed, args.penta)
    videos = list(scrape_videos(feed, conference, penta_events))
    videos.sort(key=lambda event: event.start)
    return Meta(
        conference=conference,
        videos=videos,
    )


def penta_events_by_talk_id(penta_tree):
    events = {}
    for event in penta_tree.findall('.//event'):
        conf_url = event.findtext('conf_url')
        if conf_url and conf_url.startswith('/talks/'):
            id_ = int(Path(conf_url).name)
            events[id_] = event
    return events


def scrape_videos(feed, conference, penta_events=None):
    for entry in feed['entries']:
        title = entry['title']
        id_ = int(Path(entry['link']).name)

        video = None
        for link in entry['links']:
            if not link['type'].startswith('video/'):
                continue
            video = Path(link['href']).name
        if not video:
            print('No video: {}'.format(title))
            continue

        event = Event(
            title=title,
            description=entry['description'],
            details_url=entry['link'],
            video=video,
            alt_formats={
                'lq': 'lq/{}'.format(video),
            }
        )

        if 'published_parsed' in entry:
            event.start = datetime.fromtimestamp(
                mktime(entry['published_parsed']))

        if penta_events:
            penta_event = penta_events[id_]

            description = [
                penta_event.findtext('subtitle'),
                penta_event.findtext('abstract'),
                penta_event.findtext('description'),
            ]
            description = '\n\n'.join(
                section.strip() for section in description if section)
            event.description = description.replace('\r\n', '\n')

            event.speakers = [
                person.text for person in penta_event.iter('person')]
            event.room = penta_event.findtext('room')
            # RSS published date isn't very accurate
            event.start = dateutil.parser.parse(penta_event.findtext('date'))
            event.start = event.start.replace(tzinfo=None)
            event.end = event.start + parse_timedelta(
                penta_event.findtext('duration'))

        if conference.series == 'DebConf':
            if conference.edition == 17:
                vp8 = video[:-6] + '8.webm'
                event.alt_formats['lq'] = 'lq/{}'.format(vp8)
                event.alt_formats['vp8'] = vp8
        yield event


def scrape_conference(feed, penta_url):
    title = feed['feed']['title']
    edition = int(re.search(r'Deb[Cc]onf\s*(\d+)', title).group(1))
    year = 2000 + edition

    conf = Conference(
        title=title,
        series='DebConf',
        edition=edition,
        website='https://debconf{}.debconf.org/'.format(edition),
        schedule=penta_url,
        video_base='https://meetings-archive.debian.net/pub/debian-meetings/'
                   '{}/debconf{}/'.format(year, edition),
        video_formats={
            'default': VideoFormat(
                resolution='720x576',
                bitrate='1500k',
                container='matroska',
                vcodec='vp8',
                acodec='vorbis',
            ),
            'lq': VideoFormat(
                resolution='360x288',
                bitrate='500k',
                container='ogg',
                vcodec='vp8',
                acodec='vorbis',
            ),
        },
    )
    if conf.series == 'DebConf':
        if conf.edition == 17:
            conf.video_formats = {
                'default': VideoFormat(
                    resolution='1280x720',
                    bitrate='700k',
                    container='matroska',
                    vcodec='vp9',
                    acodec='vorbis',
                ),
                'vp8': VideoFormat(
                    resolution='1280x720',
                    bitrate='1m',
                    container='matroska',
                    vcodec='vp8',
                    acodec='vorbis',
                ),
                'lq': VideoFormat(
                    resolution='1280x720',
                    bitrate='260k',
                    container='ogg',
                    vcodec='vp8',
                    acodec='vorbis',
                ),
            }
    return conf


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('rss', help='RSS feed URL')
    parser.add_argument('--penta', help='Pentabarf feed URL')
    parser.add_argument('-k', '--no-verify',
                        action='store_false', dest='verify',
                        help='Disable SSL verification')
    args = parser.parse_args()

    meta = scrape(args.rss, args)

    with open('scraped.yml', 'w') as f:
        yaml_dump(meta, f)


if __name__ == '__main__':
    main()
