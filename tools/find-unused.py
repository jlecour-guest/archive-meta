#!/usr/bin/env python3
import argparse

import yaml

from utils.files import files_with_prefix, metadata_files


def find_used():
    for manifest in metadata_files():
        with manifest.open() as f:
            data = yaml.safe_load(f)
        conference = data['conference']
        prefix = ''
        if conference['video_base']:
            prefix = conference['video_base']
            if prefix.startswith('https://meetings-archive.debian.net'
                                 '/pub/debian-meetings/'):
                prefix = prefix[56:]
        videos = data['videos']
        for video in videos:
            yield prefix + video['video']
            for format_, file_ in video.get('alt_formats', {}).items():
                yield prefix + file_


def main():
    parser = argparse.ArgumentParser(
        description="Find files that aren't contained in a manifest")
    parser.add_argument('prefix', nargs='?', default='',
                        help='Restrict to files with prefix')
    args = parser.parse_args()

    used = set(find_used())

    for video in files_with_prefix(args.prefix):
        if video not in used:
            print(video)


if __name__ == '__main__':
    main()
