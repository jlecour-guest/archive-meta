#!/usr/bin/env python3

import argparse
import os
import re
import xml.etree.ElementTree as ET
from datetime import date, datetime
from pathlib import Path
from urllib.parse import urljoin

import requests

from utils.yaml import load_meta_from_dict, yaml_dump


def parse_date(datestr):
    return date(*[int(part) for part in datestr.split('-')])


def parse_datetime(datestr):
    return datetime.strptime(datestr, '%Y-%m-%d %H:%M:%S+00')


def split_speakers(speakers):
    speakers = ','.join(speakers)
    for speaker_or_two in speakers.split(','):
        for speaker in speaker_or_two.split(' and '):
            yield speaker.strip()


def read_dc_schedule(url):
    r = requests.get(url)
    schedule = ET.fromstring(r.content)
    by_url = {}
    for event in schedule.findall('.//event'):
        by_url[event.findtext('conf_url')] = event.findtext('description')

    return by_url


def debconf(meta, wafer_base_url):
    if not wafer_base_url:
        return
    pentabarf_url = urljoin(wafer_base_url, 'schedule/pentabarf.xml')
    schedule = read_dc_schedule(pentabarf_url)
    m = re.match(r'^https://debconf(\d+)\.debconf\.org/$', wafer_base_url)
    if m:
        edition = int(m.group(1))
        meta.conference.series = 'DebConf'
        meta.conference.edition = str(edition)
        meta.conference.location = 'UNKNOWN'
        meta.conference.website = wafer_base_url
        meta.conference.schedule = urljoin(wafer_base_url, '/schedule/')
        meta.conference.video_base = (
            'https://meetings-archive.debian.net/pub/debian-meetings/'
            '{}/DebConf{}/'.format(2000 + edition, edition))

        for video in meta.videos:
            conf_url = video.description  # Hack, we stashed this there
            if conf_url in schedule:
                video.description = schedule[video.description]
                video.details_url = urljoin(wafer_base_url, conf_url)
            else:
                del video.description


def fixup(json, wafer_base_url):
    # We parsed as JSON, dates weren't parsed
    json['conference']['date'] = [
        parse_date(datestr) for datestr in json['conference']['date']]

    # https://github.com/yoe/sreview/issues/59
    video_formats = {}
    for item in json['conference']['video_formats']:
        name = list(item.keys())[0]
        format_ = item[name]

        if isinstance(format_['bitrate'], int):
            format_['bitrate'] = str(format_['bitrate']) + 'k'
        elif format_['bitrate'] is None:
            if name == 'DebConfLQ':
                format_['bitrate'] = '256k'

        if name == 'DebConfLQ':
            name = 'lq'

        video_formats[name] = format_

    json['conference']['video_formats'] = video_formats

    for video in json['videos']:
        if wafer_base_url:
            # temporarily stash ID in description, so we can find it later
            video['description'] = video.pop('eventid')
        else:
            del video['eventid']
        if video['description'] is None:
            del video['description']
        else:
            description = video['description'].strip()
            video['description'] = re.sub(r'\s*\n\s*', '\n', description)

        video['speakers'] = list(split_speakers(video['speakers']))
        if video['speakers'] == ['']:
            del video['speakers']
        video['start'] = parse_datetime(video['start'])
        video['end'] = parse_datetime(video['end'])

        video_url = video['video']
        # Strip prefix dir, we can include it in video_base
        video_url = video_url.split('/', 1)[1]
        if video_url.endswith('.lq.webm'):
            video['alt_formats'] = {
                'lq': video_url,
            }
            video_url = video_url.replace('.lq.webm', '.webm')
        else:
            video['alt_formats'] = {
                'lq': video_url.rsplit('.', 1)[0] + '.lq.webm',
            }
        video['video'] = video_url

    json['videos'].sort(key=lambda v: (v['start'], v['room']))

    return json


def re_serialize(meta, path):
    if all(hasattr(event, 'start') for event in meta.videos):
        meta.videos.sort(key=lambda event: event.start)

    tmp_path = path.with_name('.new')
    with tmp_path.open('w', encoding='utf8') as f:
        try:
            yaml_dump(meta, f)
            f.flush()
            os.fsync(f.fileno())
            os.replace(str(tmp_path), str(path))
        finally:
            if tmp_path.exists():
                tmp_path.unlink()


def main():
    parser = argparse.ArgumentParser(
        description='Download sreview JSON, save as YAML')
    parser.add_argument('output', help='File to produce')
    parser.add_argument(
        '-u', '--url', default='https://sreview.debian.net/released',
        help='SReview /released URL')
    parser.add_argument(
        '-w', '--wafer', help='Wafer conference site URL')
    args = parser.parse_args()

    if args.wafer:
        args.wafer = args.wafer.rstrip('/') + '/'

    r = requests.get(args.url)
    if r.status_code != 200:
        raise Exception('HTTP {}'.format(r.status_code))
    json = r.json()

    json = fixup(json, args.wafer)
    meta = load_meta_from_dict(json)

    debconf(meta, args.wafer)

    re_serialize(meta, Path(args.output))


if __name__ == '__main__':
    main()
