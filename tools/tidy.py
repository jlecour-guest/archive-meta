#!/usr/bin/env python3

import argparse
import os
from pathlib import Path

from utils.files import metadata_files
from utils.yaml import load_meta, yaml_dump


def tidy(path, check_only):
    """Re-serialize path"""
    print('Tidying', path)
    with path.open(encoding='utf8') as f:
        meta = load_meta(f)

    if check_only:
        return

    if all(hasattr(event, 'start') for event in meta.videos):
        meta.videos.sort(key=lambda event: event.start)

    tmp_path = path.with_name('.new')
    with tmp_path.open('w', encoding='utf8') as f:
        try:
            yaml_dump(meta, f)
            f.flush()
            os.fsync(f.fileno())
            os.replace(str(tmp_path), str(path))
        finally:
            if tmp_path.exists():
                tmp_path.unlink()


def main():
    parser = argparse.ArgumentParser(description='Canonicalize a YAML file')
    parser.add_argument('file', nargs='*', help='File to operate on')
    parser.add_argument(
        '-a', '--all', action='store_true',
        help='Find and run on every file in the directory structure')
    parser.add_argument(
        '-c', '--check', action='store_true',
        help="Only lint, don't update anything")
    args = parser.parse_args()

    if args.file and args.all:
        parser.error("If you use --all, you can't specify any files.")

    manifests = metadata_files() if args.all else args.file

    for name in manifests:
        tidy(Path(name), args.check)


if __name__ == '__main__':
    main()
